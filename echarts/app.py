from flask import Flask, render_template, url_for

def create_app():
    app = Flask(__name__)

    @app.route('/')
    def graphcirculargexf():
        return render_template('graphcirculargexf.html')

    @app.route('/graphcircularjson')
    def graphcircularjson():
        return render_template('graphcircularjson.html')

    @app.route('/graphforce')
    def graphforce():
        return render_template('graphforce.html')

    @app.route('/graphnone')
    def graphnone():
        return render_template('graphnone.html')

    return app
