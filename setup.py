from setuptools import setup, find_packages

def read(filename):
    return [
        req.strip()
        for req
        in open(filename).readlines()
    ]

setup(
    name = "echarts",
    version="0.1.0",
    description="Echarts app",
    packages = find_packages(),
    include_package_data = True,
    install_requirements=read("requirements.txt"),
    extras_require = {
        "dev": read("requirements.dev.txt")
    }
)