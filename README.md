# Echarts

Echarts site: [https://echarts.apache.org/en/index.html](https://echarts.apache.org/en/index.html)

To install the dependencies:

```bash
$ virtualenv venv
$ source venv/bin/activate
(venv)$ pip install -r requirements.txt
```

To execute:

```bash
$ export FLASK_APP=echarts/app.py
$ export FLASK_ENV=development
$ flask run
 * Serving Flask app "echarts/app.py" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 106-749-332
```

Access `http://localhost:5000/` and you should see:

![](circular.png)